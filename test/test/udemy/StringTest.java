package test.udemy;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

class StringTest {

	@BeforeAll
	static void antesQueTodos(TestInfo info) {
		System.out.println("Ejecutando: @BeforeAll " + info.getDisplayName());
	}
	

	@BeforeEach
	void antesQueCada(TestInfo info) {
		System.out.println("Ejecutando @BeforeEach antesQueCada()");
	}
	
	@AfterEach
	void despuesQueCada(TestInfo info) {
		System.out.println("Ejecutando: despuesQueCada()");
	}
	
	@AfterAll
	static void despuesQueTodo(TestInfo info) {
		System.out.println("Ejecutando: @AfterAll " + info.getDisplayName());
	}
	
	@Test
	void testA() {
		final String[] split = "hola a todos".split(" ");
		final String[] expArray = new String[] {"hola","a","todos"};
		
		assertArrayEquals("No paso the arrays test!", expArray, split);
	}
	
	
	@Test
	void testB(TestInfo info) {
		final String[] split = "hola a todos".split(" ");
		final String[] expArray = new String[] {"hola","a","todos"};
		
		assertArrayEquals("No paso the arrays test!", expArray, split);
	}

}
